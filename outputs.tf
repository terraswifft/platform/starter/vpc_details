output "vpc_id" {
  value = try(data.aws_vpc.details.0.id, null)
}

output "vpc_cidr" {
  value = try(data.aws_vpc.details.0.cidr_block, null)
}

output "subnet_id" {
  value = try(data.aws_subnet.details.0.id, null)
}

output "subnet_ids" {
  value = try([for v in data.aws_subnet.multiple : v.id], [])
}
