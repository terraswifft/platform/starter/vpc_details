variable "vpc_id" {
  type    = string
  default = null
}

variable "vpc_name" {
  type    = string
  default = null
}

variable "subnet_names" {
  type    = list(string)
  default = []
}

variable "subnet_name" {
  type    = string
  default = null
}

variable "subnet_id" {
  type    = string
  default = null
}

variable "subnet_ids" {
  type    = list(string)
  default = []
}
variable "subnets_to_return" {
  type    = number
  default = 1
}
