
locals {
  subnets_to_return = length(try(coalescelist(var.subnet_ids, var.subnet_names), [])) > 1 ? length(coalescelist(var.subnet_ids, var.subnet_names)) : 1
}
