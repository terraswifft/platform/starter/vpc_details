data "aws_vpc" "details" {
  count = try(coalesce(var.vpc_id, var.vpc_name), "") != "" ? 1 : 0
  filter {
    name   = var.vpc_id != null ? "vpc-id" : "tag:Name"
    values = [coalesce(var.vpc_id, var.vpc_name)]
  }
}

data "aws_subnet" "details" {
  count  = (try(coalesce(var.vpc_id, var.vpc_name), "") != "" && local.subnets_to_return == 1 && try(coalesce(var.subnet_id, var.subnet_name), "") != "") ? 1 : 0
  vpc_id = data.aws_vpc.details.0.id
  filter {
    name   = var.subnet_id != null ? "subnet-id" : "tag:Name"
    values = [coalesce(var.subnet_id, var.subnet_name)]
  }
}



data "aws_subnet" "multiple" {
  vpc_id   = data.aws_vpc.details.0.id
  for_each = length(try(coalescelist(var.subnet_ids, var.subnet_names), [])) >=1 ? toset(coalescelist(var.subnet_ids, var.subnet_names)) : toset([])
  filter {
    name   = length(var.subnet_ids) > 1 ? "subnet-id" : "tag:Name"
    values = [each.value]
  }
}
